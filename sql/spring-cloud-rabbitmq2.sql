
-- ----------------------------
-- 说明：
-- 这里我们会创建4个表，分别是
-- account01 账户1的数据库
-- account02 账户2的数据库
-- message   保存消息的数据库
-- queue     队列表

-- 备注：
-- account01 和 account02 模拟不同的两个数据库的账户表，为了SQL查询方便所以放在一个库里面
-- message 和 queue 也是，在真正的项目中应该把他们放在一个专门处理消息的库
-- ----------------------------


-- ----------------------------
-- 创建数据库
-- ----------------------------

DROP DATABASE IF EXISTS `spring-cloud-rabbitmq-account`;
CREATE DATABASE `spring-cloud-rabbitmq-account`;



-- ----------------------------
-- 账户表1
-- ----------------------------
DROP TABLE IF EXISTS `spring-cloud-rabbitmq-account`.`account01`;
CREATE TABLE `spring-cloud-rabbitmq-account`.`account01` (
  `id` 				bigint(20) 		NOT NULL COMMENT '账户ID',
  `username` 		varchar(50) 	DEFAULT NULL COMMENT '用户名称',
  `money` 			decimal(10,2) 	DEFAULT NULL COMMENT '账户余额',
  `create_time` 	datetime 		DEFAULT NULL COMMENT '创建时间',
  `update_time` 	datetime 		DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账户表1' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 账户表2
-- ----------------------------
DROP TABLE IF EXISTS `spring-cloud-rabbitmq-account`.`account02`;
CREATE TABLE `spring-cloud-rabbitmq-account`.`account02` (
  `id` 				bigint(20) 		NOT NULL COMMENT '账户ID',
  `username` 		varchar(50) 	DEFAULT NULL COMMENT '用户名称',
  `money` 			decimal(10,2) 	DEFAULT NULL COMMENT '账户余额',
  `create_time` 	datetime 		DEFAULT NULL COMMENT '创建时间',
  `update_time` 	datetime 		DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '账户表2' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 去重表，保存事务ID（用于幂等功能）
-- ----------------------------
DROP TABLE IF EXISTS `spring-cloud-rabbitmq-account`.`de_duplication`;
CREATE TABLE `spring-cloud-rabbitmq-account`.`de_duplication` (
  `tx_id` 					bigint(20) 		NOT NULL COMMENT '事务ID',
  PRIMARY KEY (`tx_id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '去重表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 消息表
-- ----------------------------
DROP TABLE IF EXISTS `spring-cloud-rabbitmq-account`.`message`;
CREATE TABLE `spring-cloud-rabbitmq-account`.`message` (
   `id` 				bigint(20) 		NOT NULL COMMENT '充值记录ID',
   `consumer_queue` 	varchar(200) 	DEFAULT NULL COMMENT '消费队列',
   `message_body` 	longtext 		DEFAULT NULL COMMENT '消息内容',
   `resend_times` 	int(11) 		DEFAULT NULL COMMENT '重发次数',
   `already_dead` 	smallint(11) 	DEFAULT NULL COMMENT '是否已死（0:否、1:是）',
   `status` 			tinyint(2) 		DEFAULT '0' COMMENT  '消息状态（0:未发送 1:已发送）',
   `confirm_time` 	datetime 		DEFAULT NULL COMMENT '确认时间',
   `create_time` 	datetime 		DEFAULT NULL COMMENT '创建时间',
   `update_time` 	datetime 		DEFAULT NULL COMMENT '更新时间',
   PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '消息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- 队列表
-- ----------------------------
DROP TABLE IF EXISTS `spring-cloud-rabbitmq-account`.`queue`;
CREATE TABLE `spring-cloud-rabbitmq-account`.`queue` (
    `id` 				bigint(20) 		NOT NULL COMMENT '充值记录ID',
    `business_name` 	varchar(200) 	DEFAULT NULL COMMENT '微服务名称',
    `consumer_queue` 	varchar(200) 	DEFAULT NULL COMMENT '消费队列',
    `check_url` 		varchar(500) 	DEFAULT NULL COMMENT '消息确认URL，消息确认子系统与业务系统确认是否发送消息的接口地址',
    `check_duration` 	int(11) 		DEFAULT NULL COMMENT '确认条件（毫秒），也就是多长时间未进行确认的消息需要进行确认',
    `check_timeout` 	int(11) 		DEFAULT NULL COMMENT '确认超时时长（毫秒），也就是HTTP请求超时时长',
    `create_user` 	varchar(200) 	    DEFAULT NULL COMMENT '创建者',
    `create_time` 	datetime 		    DEFAULT NULL COMMENT '创建时间',
    `update_user` 	varchar(200) 	    DEFAULT NULL COMMENT '更新者',
    `update_time` 	datetime 		    DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '队列表' ROW_FORMAT = Dynamic;
