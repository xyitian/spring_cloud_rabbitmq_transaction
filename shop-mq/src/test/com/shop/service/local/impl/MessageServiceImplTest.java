package com.shop.service.impl;

import com.shop.service.local.MessageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class MessageServiceImplTest {

    @Autowired
    MessageService messageService;

    @Test
    void add() {

    }

    @Test
    void addWaitSendMessage() {
        String consumerQueue = "消息队列";
        String messageBody = "消息内容";

        Long result = messageService.addWaitSendMessage(consumerQueue, messageBody);
        System.out.println(result );
    }


    @Test
    void get() {
    }

    @Test
    void findList() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
        Long id = 6920863456384L;
        Integer result = messageService.delete(id);
        System.out.println(1 == result);
    }
}