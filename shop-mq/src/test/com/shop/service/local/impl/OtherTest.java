package com.shop.service.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.google.gson.Gson;
import com.shop.entity.Message;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

class OtherTest {

    @Test
    void syncSend() throws InterruptedException {

        // 计算时间
        LocalDateTime endTime = LocalDateTime.now().minus(18000, ChronoUnit.MILLIS);
        System.out.println(new Gson().toJson(endTime));

        Date datetime = DateUtil.offset(new Date(), DateField.MILLISECOND, 18000);
        System.out.println(new Gson().toJson(datetime));
        System.out.println(new Gson().toJson(new Date()));
    }

    @Test
    void syncSend2() throws InterruptedException {
        Message message = new Message();
        message.setResendTimes(0);

        message.setResendTimes(message.getResendTimes() + 1);

        System.out.println(message.getResendTimes());
    }


}