package com.shop.service.impl;

import com.shop.ShopMqApplication;
import com.shop.entity.Message;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ShopMqApplication.class)
class RabbitTemplateTest {

    @Autowired
    RabbitTemplate rabbitTemplate;

    private Logger logger = LoggerFactory.getLogger(Logger.class);



    /**
     * 这个测试类是为了验证以下问题：
     *      （一）到底是 TopicExchange 的 bindingKey 的规则去匹配 Queue 的 routingKey
     *      （二）还是 Queue 的 routingKey 的规则去匹配 TopicExchange 的 bindingKey
     *
     * 实验结果：Exchange 就像一个邮差一样，Queue 就是送货地址，所以地址是由 Queue设定的。是 Exchange 去满足 Queue设置的规则。
     * 结论：
     *      交换机：相当于一个邮箱
     *      RoutingKey：相当于邮件包裹的地址
     *      BindingKey：相当于目的地
     *
     * 是 RabbitMQ 的交换机绑定队列时的 RoutingKey 匹配我们在发送时设置的地址
     * 例如，我的交换机绑定了队列 com.order.topic.one，然后设置了 Routing Key 为 com.order.#
     * 那么我在使用 rabbitTemplate.convertAndSend() 方法发信息的时候，例如下面3个例子：
     *      rabbitTemplate.convertAndSend("order-topic-exchange", "com.order.aaa", order);
     *      rabbitTemplate.convertAndSend("order-topic-exchange", "com.order.bbb.two", order);
     *      rabbitTemplate.convertAndSend("order-topic-exchange", "com.order.topic.two", order);
     * 这三个地址的信息都能发送到队列 com.order.topic.one 上面
     */



    /**
     * 先来验证问题（一）
     * 例如有3个 Queue，Queue的名称和routingKey都一样，分别是下面3个：
     * com.order.topic.one
     * com.order.topic.two
     * com.order.topic.three
     *
     * 有一个通配符交换机，名字为 order-topic-exchange，bindingKey 为 com.order.#
     * 接下来我们实验，看看交换机使用 bindingKey 为 com.order.# 去发送消息，看看这三个队列能否收到信息
     * 经过测试：使用 com.order.# 或者 com.order.* 发送信息时，MQ都收不到信息哦，别说3个队列了
     *
     */
    @Test
    void syncSend() throws InterruptedException {
        Message message = new Message();
        message.setId(666L);
        message.setConsumerQueue("consumerQueue");
        message.setMessageBody("messageBody");
        message.setResendTimes(0);  // 重发次数
        message.setAlreadyDead(0);  // 是否已死（0:否、1:是）
        message.setStatus(0);       // 消息状态（0:未发送 1:已发送）
        message.setConfirmTime(null);
        Date now = new Date();
        message.setCreateTime(now);
        message.setUpdateTime(now);

        /**
         * exchange（交换机）
         * routingKey（路由键）
         * object （消息内容）
         * correlationData（消息唯一id）
         */
        rabbitTemplate.convertAndSend("shop-account-topic-exchange", "shop-account.transfer.queue", message);

        logger.info("消息发送完毕");
        /*
        order = new Order();
        order.setId(UUID.randomUUID().toString());
        order.setName("通配符交换机 syncSend 方法的订单，routingKey = com.order.#");
        order.setMessageId(String.valueOf(System.currentTimeMillis()));
        rabbitTemplate.convertAndSend("order-topic-exchange", "com.order.#", order);*/
    }


}