package com.shop.controller;

import com.github.pagehelper.PageInfo;
import com.shop.common.Result;
import com.shop.common.ResultGenerator;
import com.shop.entity.Message;
import com.shop.service.MessageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 独舟
 * @description 消息表api接口
 * @version 2020-05-01
 */
@RestController
@Api(value = "消息表接口", tags = "消息表")
@RequestMapping(value = "/message")
public class MessageController {

    @Autowired
    MessageService messageService;

    @ApiOperation(value = "新增消息表")
    @PostMapping(value = "/add")
    public Result add(@RequestBody Message message){
        messageService.add(message);
        return ResultGenerator.success();
    }

    @ApiOperation(value = "根据Id查询消息表详情")
    @GetMapping(value = "/get")
    public Result<Message> get(Long id){
        return ResultGenerator.success(messageService.get(id));
    }

    @ApiOperation(value = "分页查询消息表")
    @PostMapping(value = "/findList")
    public Result<PageInfo<List<Message>>> findList(@RequestBody Message message,
                                                    @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                                    @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        return ResultGenerator.success(messageService.findList(message, pageNum, pageSize));
    }

    @ApiOperation(value = "根据Id修改消息表")
    @PostMapping(value = "update")
    public Result update(@RequestBody Message message){
        messageService.update(message);
        return ResultGenerator.success();
    }

    @ApiOperation(value = "删除消息表")
    @GetMapping(value = "/delete")
    public Result delete(Long id){
        messageService.delete(id);
        return ResultGenerator.success();
    }

}