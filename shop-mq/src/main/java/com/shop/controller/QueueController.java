package com.shop.controller;

import com.github.pagehelper.PageInfo;
import com.shop.common.Result;
import com.shop.common.ResultGenerator;
import com.shop.entity.Queue;
import com.shop.service.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.List;

/**
 * @author 独舟
 * @description 消息队列api接口
 * @version 2020-05-01
 */
@RestController
@Api(value = "消息队列接口", tags = "消息队列")
@RequestMapping(value = "/queue")
public class QueueController {

    @Autowired
    QueueService queueService;

    @ApiOperation(value = "新增消息队列")
    @PostMapping(value = "/add")
    public Result add(Queue queue){
        queueService.add(queue);
        return ResultGenerator.success();
    }

    @ApiOperation(value = "根据Id查询消息队列详情")
    @PostMapping(value = "/get")
    public Result<Queue> get(Long id){
        return ResultGenerator.success(queueService.get(id));
    }

    @ApiOperation(value = "分页查询消息队列")
    @PostMapping(value = "/findList")
    public Result<PageInfo<List<Queue>>> findList(@RequestBody Queue queue,
                                                  @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                                                  @RequestParam(required = false, defaultValue = "10") Integer pageSize){
        return ResultGenerator.success(queueService.findList(queue, pageNum, pageSize));
    }

    @ApiOperation(value = "根据Id修改消息队列")
    @PostMapping(value = "update")
    public Result update(@RequestBody Queue queue){
        queueService.update(queue);
        return ResultGenerator.success();
    }

    @ApiOperation(value = "删除消息队列")
    @PostMapping(value = "/delete")
    public Result delete(Long id){
        queueService.delete(id);
        return ResultGenerator.success();
    }

}