package com.shop.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.shop.dao.QueueDao;
import com.shop.entity.Queue;
import com.shop.service.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class QueueServiceImpl implements QueueService {

    @Autowired
    QueueDao queueDao;

    /**
     * 新增消息队列
     * @param queue
     * @return java.lang.Integer
     */
    @Override
    @Transactional
    public Integer add(Queue queue) {
        return queueDao.insert(queue);
    }

    /**
     * 根据Id查询消息队列
     * @param id
     * @return com.shop.entity.Queue
     */
    @Override
    public Queue get(Long id) {
        return queueDao.selectById(id);
    }

    /**
     * 分页查询消息队列
     * @param queue
     * @return java.util.List<com.shop.entity.Queue>
     */
    @Override
    public PageInfo<List<Queue>> findList(Queue queue, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<Queue> list = queueDao.selectList(queue);
        PageInfo pageInfo = new PageInfo(list);
        return pageInfo;
    }

    /**
     * 根据Id修改消息队列
     * @param queue
     * @return java.lang.Integer
     */
    @Override
    @Transactional
    public Integer update(Queue queue) {
        return queueDao.updateById(queue);
    }

    /**
     * 根据Id删除消息队列
     * @param id
     * @return java.lang.Integer
     */
    @Override
    @Transactional
    public Integer delete(Long id) {
        return queueDao.deleteById(id);
    }
	
}