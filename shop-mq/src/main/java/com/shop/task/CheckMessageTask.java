package com.shop.task;

import com.shop.service.MessageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 检查未确认的消息的定时任务
 * @author duzhou
 */
@Component
@Slf4j
public class CheckMessageTask {

    @Autowired
    MessageService messageService;

    /**
     * 消息确认功能，处理所有长时间未确认的消息，和上游业务系统确认是否发送该消息
     */
    @Scheduled(cron = "0 */1 * * * ? ")
    public void CheckUnConfirmMessageTask() {
        log.info("");
        log.info("定时任务：检查未确认的消息 begin");
        messageService.checkWaitingMessage();
        log.info("定时任务：检查未确认的消息 end");
    }

    /**
     * 消息恢复功能，对于长时间未消费的消息，重新发送消息给下层业务系统
     */
    @Scheduled(cron = "0 */1 * * * ? ")
    public void CheckUnConsumeMessageTask() {
        log.info("");
        log.info("定时任务：检查未消费的消息 begin");
        messageService.checkUnConsumeMessage();
        log.info("定时任务：检查未消费的消息 end");
    }

    /**
     * 标记消息死亡，对于到达重发次数上限的消息设置为死亡
     */
    @Scheduled(cron = "0 */1 * * * ? ")
    public void updateMessageDead() {
        log.info("");
        log.info("定时任务：标记消息死亡 begin");
        messageService.updateMessageDead();
        log.info("定时任务：标记消息死亡 end");
    }

}
