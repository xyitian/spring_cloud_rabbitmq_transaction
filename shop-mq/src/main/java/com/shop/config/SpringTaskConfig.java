package com.shop.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * 定时任务配置
 *
 * @EnableScheduling 开启定时任务功能，这个注解也可以放在启动类，但是为了启动类的简洁所以配置在这里
 * @author duzhou
 */
@Configuration
@EnableScheduling
public class SpringTaskConfig {

}
