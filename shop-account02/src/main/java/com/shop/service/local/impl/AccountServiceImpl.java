package com.shop.service.local.impl;

import com.shop.common.CustomException;
import com.shop.dao.AccountDao;
import com.shop.entity.Account;
import com.shop.service.local.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.math.BigDecimal;

/**
 * @author duzhou
 */
@Service
public class AccountServiceImpl implements AccountService {

    private Logger logger = LoggerFactory.getLogger(getClass().getName());

    @Autowired
    AccountDao accountDao;

    /**
     * 根据用户id添加账户额度
     * @param id        账户id
     * @param money     添加的额度
     * @return java.lang.Integer
     */
    @Override
    @Transactional
    public Integer addMoney(Long id, BigDecimal money) {

        if (null == id) {
            throw new CustomException("账户id不能为空");
        }
        if (null == money) {
            throw new CustomException("money不能为空");
        }

        Account before = accountDao.selectById(id);

        // 判断传过来的userId是否正确
        if (null == before) {
            throw new CustomException("找不到用户账户，请检查参数是否正确");
        }

        // 添加余额
        BigDecimal balance = before.getMoney().add(money);
        before.setMoney(balance);
        Integer result = accountDao.updateById(before);

        return result;
    }
}