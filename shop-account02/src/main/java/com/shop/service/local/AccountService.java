package com.shop.service.local;

import java.math.BigDecimal;

/**
 * @author  独舟
 * @description 账户表业务逻辑接口
 * @date 2020-04-20
 */
public interface AccountService {

    /**
     * 根据用户id添加账户额度
     * @param id        账户id
     * @param money     添加的额度
     * @return java.lang.Integer
     */
    Integer addMoney(Long id, BigDecimal money);
}