package com.shop.dao;

import com.shop.entity.Account;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author 独舟
 * @description 账户表api接口
 * @version 2020-04-20
 */
@Repository
public interface AccountDao {

    /**
     * 插入账户表
     * @param account
     * @return java.lang.Integer
     */
    Integer insert(Account account);

    /**
     * 根据ID查询账户表
     * @param id
     * @return com.shop.entity.Account
     */
    Account selectById(@Param("id") Long id);

    /**
     * 根据ID修改账户表
     * @param account
     * @return java.lang.Integer
     */
    Integer updateById(Account account);
}