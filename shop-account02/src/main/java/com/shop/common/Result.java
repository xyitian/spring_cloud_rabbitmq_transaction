package com.shop.common;


import com.google.gson.Gson;

/**
 * 统一API响应结果封装
 *
 * 使用泛型的优点：
 *    1. 类型安全：泛型指定了类型，如果放错了类型编译器会报错，如果不使用泛型，任何类型都可以放进来
 *    2. 避免装箱拆箱带来的性能损耗
 */
public class Result<T> {
    private int code;
    private String message;
    private T data;

    public Result setCode(ResultCode resultCode) {
        this.code = resultCode.code();
        return this;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public Result setData(T data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
