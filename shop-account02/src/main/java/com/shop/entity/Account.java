package com.shop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author 独舟
 * @description 账户表信息
 * @version 2020-04-20
 */
@Data
@ApiModel(value = "账户表信息")
public class Account implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "账户ID，最大长度20", required = true)
    private Long id;
    @ApiModelProperty(value = "用户名称，最大长度50", required = false)
    private String username;
    @ApiModelProperty(value = "账户余额，最大长度0", required = false)
    private BigDecimal money;
    @ApiModelProperty(value = "创建时间，最大长度0", required = false)
    private Date createTime;
    @ApiModelProperty(value = "更新时间，最大长度0", required = false)
    private Date updateTime;
    @JsonIgnore
    @ApiModelProperty(value = "事务ID，最大长度20", required = false)
    private Long txId;
}