package com.shop.dao;
import java.math.BigDecimal;
import java.util.Date;

import com.shop.entity.Account;
import com.shop.util.SnowFlake;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AccountDaoTest {

    @Autowired
    AccountDao accountDao;
    @Autowired
    SnowFlake snowFlake;

    @Test
    void insert() {
        Account account = new Account();
        account.setId(snowFlake.nextId());
        account.setUsername("李四");
        account.setMoney(new BigDecimal("0"));
        account.setCreateTime(new Date());
        account.setUpdateTime(new Date());

        accountDao.insert(account);

        Account newAccount = accountDao.selectById(account.getId());
        Assert.assertEquals(newAccount.getUsername(), account.getUsername());

        newAccount.setUpdateTime(new Date());

        Integer result = accountDao.updateById(newAccount);
    }

    @Test
    void selectById() {
    }

    @Test
    void updateById() {
    }
}