package com.shop.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * @author 独舟
 * @description 事务表Dao
 * @version 2020-04-20
 */
@Repository
public interface DeDuplicationDao {

    /**
     * 插入事务表
     * @param txId
     * @return java.lang.Integer
     */
    Integer insert(@Param("txId") Long txId);

    /**
     * 根据ID查询事务表
     * @param txId
     * @return com.shop.entity.Account
     */
    Integer selectCountByTxId(@Param("txId") Long txId);
}