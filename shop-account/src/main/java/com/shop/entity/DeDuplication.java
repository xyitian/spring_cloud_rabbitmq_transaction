package com.shop.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * @author 独舟
 * @description 事务信息
 * @version 2020-04-20
 */
@Data
@ApiModel(value = "事务信息")
public class DeDuplication implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "事务ID，最大长度20", required = true)
    private Long txId;
}