package com.shop.controller;

import com.shop.common.Result;
import com.shop.common.ResultGenerator;
import com.shop.controller.intoparam.TransferParam;
import com.shop.service.local.AccountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 独舟
 * @description 账户api接口
 * @version 2020-04-20
 */
@Api(value = "账户接口", tags = "账户")
@RestController
@RequestMapping(value = "/account")
public class AccountController {

    @Autowired
    AccountService accountService;

    /**
     * @param transferParam 转账入参
     * @return
     */
    @ApiOperation(value = "模拟银行支付回调")
    @PostMapping(value = "/transfer")
    public Result transfer(@RequestBody TransferParam transferParam){
         accountService.transfer(transferParam);
        return ResultGenerator.success();
    }

}