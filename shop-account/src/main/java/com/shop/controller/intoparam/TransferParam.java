package com.shop.controller.intoparam;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author 独舟
 * @description 转账入参
 * @version 2020-04-29
 */
@Data
@ApiModel(value = "转账入参")
public class TransferParam implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "转出账户ID，最大长度20", required = true)
	private Long id;
    @ApiModelProperty(value = "转出金额", required = true)
	private BigDecimal money;
    @ApiModelProperty(value = "转入账户ID，最大长度20", required = true)
    private Long targetId;

}