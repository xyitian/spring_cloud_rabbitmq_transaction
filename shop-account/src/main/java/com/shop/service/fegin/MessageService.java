package com.shop.service.fegin;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author  独舟
 * @description 消息表业务逻辑接口
 * @date 2020-05-01
 */
@FeignClient(value = "shop-mq")
public interface MessageService {

    /**
     * 添加预发送消息
     * @param consumerQueue 消费者队列
     * @param messageBody   消息内容
     * @return
     */
    @PostMapping("/message-open-api/addWaitSendMessage")
    Long addWaitSendMessage(@RequestParam("consumerQueue") String consumerQueue,
                            @RequestParam("messageBody") String messageBody);

    /**
     * 确认并发送消息
     * @param messageId 消息id
     */
    @PostMapping("/message-open-api/confirmAndSendMessage")
    void confirmAndSendMessage(@RequestParam("messageId") Long messageId);

    /**
     * 根据Id删除消息表
     * @param id
     * @return java.lang.Integer
     */
    @PostMapping("/message-open-api/delete")
    Integer delete(@RequestParam("id") Long id);
}