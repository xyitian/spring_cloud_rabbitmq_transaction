package com.shop.service.local;

import com.shop.controller.intoparam.TransferParam;
import com.shop.entity.Account;

import java.math.BigDecimal;

/**
 * @author  独舟
 * @description 账户表业务逻辑接口
 * @date 2020-04-20
 */
public interface AccountService {

    /**
     * 转账
     * @param transferParam
     * @return java.lang.Integer
     */
    void transfer(TransferParam transferParam);
}