package com.shop.dao;

import com.shop.util.SnowFlake;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DeDuplicationDaoTest {

    @Autowired
    DeDuplicationDao deDuplicationDao;
    @Autowired
    SnowFlake snowFlake;

    @Test
    void insert() {
        Long txId = snowFlake.nextId();
        Integer result = deDuplicationDao.insert(txId);
        System.out.println(1 == result);

        Integer count = deDuplicationDao.selectCountByTxId(txId);
        System.out.println(count >= 1);
    }

    @Test
    void selectById() {
    }
}